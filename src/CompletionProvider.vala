namespace Editor {
	public class Item : GLib.Object {
		public Afrodite.Symbol? symbol { get; construct; }
		public Afrodite.DataType? datatype { get; construct; }
		public Item parent { get; private set; }
		
		public Item (Afrodite.Symbol? symbol = null, Afrodite.DataType? datatype = null) {
			GLib.Object (symbol: symbol, datatype: datatype);
		}
		
		construct {
		//	if (symbol != null) parent = new Item (symbol.parent);
		}
		
		public bool is_root {
			get {
				return symbol != null && symbol.is_root;
			}
		}
		
		public bool is_instance {
			get {
				return true;
			}
		}
	}
	
	public class CompletionProvider : Afrodite.CompletionEngine, Gtk.SourceCompletionProvider {
		public CompletionProvider() {
			base ("Editor");
		}

		Regex member_access;
		Regex member_access_split;
		Regex function_call;

		construct {
			member_access = new Regex("""((?:\w+(?:\s*\([^()]*\))?\.)*)(\w*)$""");
			member_access_split = new Regex ("""(\s*\([^()]*\))?\.""");
			function_call = new Regex("""(new )?((?:\w+(?:\s*\([^()]*\))?\.)*)(\w+)\s*\(([^(,)]+,)*([^(,)]*)$""");
			end_parsing.connect (sender => { print ("fini !\n"); });
			file_parsed.connect ((sender, filename, result) => {
				foreach (var msg in result.error_messages)
					print ("Error ! : %s\n", msg);
			});
		}

		public Document document { get; set; }

		public void set_source (string filename) {
			var sf = codedom.lookup_source_file (filename);
			if (sf != null)
				remove_source_filename (filename);
			queue_sourcefile (filename);
		}

		public void set_package (string pkg) {
			var list = Afrodite.Utils.get_package_paths (pkg);
			if (list != null && list.size > 0) {
				foreach (var path in list) {
					var sf = codedom.lookup_source_file (path);
					if (sf == null)
						queue_sourcefile (path, null, true);
				}
			}
		}

		public string get_name() {
			return "Editor";
		}

		public bool activate_proposal (Gtk.SourceCompletionProposal proposal, Gtk.TextIter iter) {
			return false;
		}
		
		Afrodite.Symbol get_symbol_for_name (string type_name) {
			foreach (var key in codedom.symbols.get_keys())
				if (type_name in codedom.symbols[key].fully_qualified_name)
					return codedom.symbols[key];
			return codedom.root;
		}

		Item get_parent_item (string text, int line, int column) {
			var array = member_access_split.split (text);
			Item parent = new Item (codedom.root);
			if (array.length < 2)
				return parent;
			for (var i = 0; i < array.length; i++) {
				if (i == 0) {
					var sym = codedom.lookup_symbol_at (document.location, line, column);
					if (sym != null && sym.local_variables != null)
						foreach (var dt in sym.local_variables) {
							if (dt.symbol != null && dt.name == array[i]) {
								parent = new Item (null, dt);
								continue;
							}
						}
					if (sym != null && sym.parameters != null)
						foreach (var dt in sym.parameters) {
							if (dt.symbol != null && dt.name == array[i]) {
								parent = new Item (null, dt);
								continue;
							}
						}
					var psym = parent_symbol (sym);
					foreach (var child in psym.children)
						if (child.name == array[0] && !child.is_static) {
							parent = new Item (child);
							continue;
						}
				}
				if (parent.symbol != null && (parent.symbol.member_type == Afrodite.MemberType.FIELD || parent.symbol.member_type == Afrodite.MemberType.PROPERTY)) {
					foreach (var child in parent.symbol.symbol_type.symbol.children)
						if (child.name == array[i]) {
							parent = new Item (child);
							break;
						}
				}
				else if (parent.datatype != null) {
					foreach (var child in base_type_symbols (parent.datatype.symbol))
						if (!child.is_static && child.name == array[i]) {
							parent = new Item (child);
						}
					foreach (var child in parent.datatype.symbol.children)
						if (child.name == array[i]) {
							parent = new Item (child);
							break;
						}
				}
				else if (parent.symbol != null)
					foreach (var child in parent.symbol.children)
						if (child.name == array[i]) {
							parent = new Item (child);
							break;
						}
			}
			return parent;
		}
		
		Afrodite.Symbol parent_symbol (Afrodite.Symbol symbol) {
			if (symbol.parent != null)
				return symbol.parent;
			string p = symbol.fully_qualified_name.substring (0, symbol.fully_qualified_name.last_index_of ("."));
			if (codedom.symbols[p] != null)
				return codedom.symbols[p];
			return codedom.root;
		}
		
		public void populate (Gtk.SourceCompletionContext context) {
			Gtk.TextIter iter;
			document.view.buffer.get_iter_at_line_offset (out iter, context.iter.get_line (), 0);
			string text = document.view.buffer.get_slice (iter, context.iter, true);
			int line = context.iter.get_line();
			int column = context.iter.get_line_offset();
			MatchInfo match_info;
			member_access.match (text, 0, out match_info);
			var item = get_parent_item (match_info.fetch (1), line, column);
			var hashset = new Vala.HashSet<Gtk.SourceCompletionProposal>(direct_hash, (s1, s2) => {
				return str_equal ((s1 as Proposal).symbol.name, (s2 as Proposal).symbol.name);
			});
			var sf = codedom.lookup_source_file (document.location);
			if (sf != null && sf.using_directives != null)
				foreach (var ud in sf.using_directives)
					if (ud.symbol != null)
						foreach (var child in ud.symbol.children) {
							if (child.name.index_of (match_info.fetch (2)) == 0)
								hashset.add (new SymbolItem (child));
						}
			var lsym = codedom.lookup_symbol_at (document.location, line, column);
			if (item.is_root && lsym != null) {
				if (lsym.local_variables != null)
					foreach (var dt in lsym.local_variables) {
						var g = get_symbol_for_name (dt.type_name);
						if (dt.name.index_of (match_info.fetch (2)) == 0)
							hashset.add (new DataTypeItem (dt, g));
					}
				if (lsym.parameters != null)
					foreach (var dt in lsym.parameters) {
						var g = get_symbol_for_name (dt.type_name);
						if (dt.name.index_of (match_info.fetch (2)) == 0)
							hashset.add (new DataTypeItem (dt, g));
					}
				var sym = parent_symbol (lsym);
				foreach (var child in sym.children)
					if (child.name.index_of (match_info.fetch (2)) == 0)
						hashset.add (new SymbolItem (child));
			}
			
			if (item.datatype != null) {
				var sym = item.datatype.symbol;
				foreach (var symbol in base_type_symbols (sym)) {
					if ((symbol.member_type == Afrodite.MemberType.PROPERTY ||
						symbol.member_type == Afrodite.MemberType.METHOD ||
						symbol.member_type == Afrodite.MemberType.SIGNAL ||
						symbol.member_type == Afrodite.MemberType.FIELD) && !symbol.is_static) {
							if (symbol.name.index_of (match_info.fetch (2)) == 0)
								hashset.add (new SymbolItem (symbol));
						}
				}
				foreach (var child in sym.children)
					if (child.name.index_of (match_info.fetch (2)) == 0 && !child.is_static)
						hashset.add (new SymbolItem (child));
			}
			else if (item.symbol != null) {
				var sym = item.symbol;
				var member_type = sym.member_type;
				foreach (var symbol in base_type_symbols (sym)) {
					if ((member_type == Afrodite.MemberType.LOCAL_VARIABLE ||
							member_type == Afrodite.MemberType.NONE ||
							member_type == Afrodite.MemberType.PROPERTY ||
							member_type == Afrodite.MemberType.FIELD ||
							member_type == Afrodite.MemberType.ENUM ||
							member_type == Afrodite.MemberType.ERROR_DOMAIN ||
							member_type == Afrodite.MemberType.METHOD ||
							member_type == Afrodite.MemberType.SIGNAL ||
							member_type == Afrodite.MemberType.PARAMETER) && !symbol.is_static) {
							if (symbol.name.index_of (match_info.fetch (2)) == 0)
								hashset.add (new SymbolItem (symbol));
						}
				}
				if (member_type == Afrodite.MemberType.PROPERTY ||
					member_type == Afrodite.MemberType.FIELD) {
					foreach (var child in sym.symbol_type.symbol.children)
						if (child.member_type != Afrodite.MemberType.SCOPED_CODE_NODE && 
							child.name.index_of (match_info.fetch (2)) == 0 && !child.is_static)
							hashset.add (new SymbolItem (child));
				}
				foreach (var child in sym.children) {
					if (member_type == Afrodite.MemberType.NAMESPACE) {
						if (child.name.index_of (match_info.fetch (2)) == 0)
							hashset.add (new SymbolItem (child));
					}
					if ((member_type == Afrodite.MemberType.NONE ||
						member_type == Afrodite.MemberType.CLASS ||
						member_type == Afrodite.MemberType.STRUCT) && child.is_static)
							if (child.name.index_of (match_info.fetch (2)) == 0)
								hashset.add (new SymbolItem (child));
					if ((member_type == Afrodite.MemberType.LOCAL_VARIABLE ||
							member_type == Afrodite.MemberType.NONE ||
							member_type == Afrodite.MemberType.PROPERTY ||
							member_type == Afrodite.MemberType.FIELD ||
							member_type == Afrodite.MemberType.ENUM ||
							member_type == Afrodite.MemberType.ERROR_DOMAIN ||
							member_type == Afrodite.MemberType.METHOD ||
							member_type == Afrodite.MemberType.SIGNAL ||
							member_type == Afrodite.MemberType.PARAMETER) && !child.is_static) {
							if (child.name.index_of (match_info.fetch (2)) == 0)
								hashset.add (new SymbolItem (child));
						}
				}
			}
			
			var list = new List<Gtk.SourceCompletionProposal>();
			foreach (var si in hashset)
				list.append (si);
			context.add_proposals (this, list, true);
		}
		
		Vala.HashSet<Afrodite.Symbol> base_type_symbols (Afrodite.Symbol symbol) {
			var hset = new Vala.HashSet<Afrodite.Symbol> (direct_hash, (s1, s2) => {
				return str_equal (((Afrodite.Symbol)s1).name, ((Afrodite.Symbol)s2).name);
			});
			if (symbol.base_types != null)
				foreach (var dt in symbol.base_types) {
					foreach (var sym in base_type_symbols (dt.symbol))
						hset.add (sym);
					foreach (var child in dt.symbol.children)
						hset.add (child);
				}
			if (symbol.symbol_type != null && symbol.symbol_type.symbol != null && symbol.symbol_type.symbol.base_types != null)
				foreach (var dt in symbol.symbol_type.symbol.base_types) {
					foreach (var sym in base_type_symbols (dt.symbol))
						hset.add (sym);
					foreach (var child in dt.symbol.children)
						hset.add (child);
				}
			return hset;
		}
		
	}
	
	public class Proposal : Gtk.SourceCompletionProposal, GLib.Object {
		public Afrodite.Symbol symbol { get; set; }
		public string name { get; set; }
		public Gdk.Pixbuf icon { get; set; }
		
		public string get_info() {
			return symbol.info;
		}
		public string get_label() {
			return name;
		}
		public string get_markup() {
			return name;
		}
		public string get_text() {
			return name;
		}
		public unowned Gdk.Pixbuf get_icon() {
			return icon;
		}
	}

	public class SymbolItem : Proposal {
		public SymbolItem (Afrodite.Symbol symbol) {
			this.symbol = symbol;
			var klass = (EnumClass)typeof (Afrodite.MemberType).class_ref();
			var val = klass.get_value (symbol.member_type);
			icon = new Gdk.Pixbuf.from_resource ("/resources/icons/%s.png".printf (val.value_nick));
			name = symbol.name;
		}
	}

	public class DataTypeItem : Proposal {
		public DataTypeItem (Afrodite.DataType datatype, Afrodite.Symbol symbol) {
			this.symbol = symbol;
			var dt = datatype;
			var klass = (EnumClass)typeof (Afrodite.MemberType).class_ref();
			var val = klass.get_value (symbol.member_type);
			icon = new Gdk.Pixbuf.from_resource ("/resources/icons/%s.png".printf (val.value_nick));
			name = dt.name;
		}
	}

}
