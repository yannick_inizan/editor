namespace Editor {
	public class FileChooserDialog : Gtk.FileChooserDialog {
		public FileChooserDialog (string title, Gtk.Window parent, Gtk.FileChooserAction action) {
			GLib.Object (use_header_bar: 1, title: title, transient_for: parent, action: action, create_folders: true);
		}
		
		construct {
			add_buttons ("Cancel", Gtk.ResponseType.CANCEL, "OK", Gtk.ResponseType.OK);
		}
	}
	
	[GtkTemplate (ui = "/resources/CreationWizard.ui")]
	public class CreationWizard : Gtk.Assistant {
		[GtkChild]
		Gtk.ScrolledWindow packages_sw;
		[GtkChild]
		Gtk.Box path_box;
		[GtkChild]
		Gtk.Entry path_entry;
		[GtkChild]
		Gtk.Entry name_entry;
		[GtkChild]
		Gtk.SpinButton major_spin;
		[GtkChild]
		Gtk.SpinButton minor_spin;
		[GtkChild]
		Gtk.SpinButton patch_spin;
		
		PackageView view;
		
		public CreationWizard() {
			GLib.Object (use_header_bar: 1);
		}
		
		construct {
			view = new PackageView();
			packages_sw.add (view);
			name_entry.editable = false;
			view.selection_changed.connect (() => {
				project.packages = view.packages;
			});
		}
		
		[GtkCallback]
		void on_spin_value_changed() {
			var version = new Project.Version();
			version.major = (uint)major_spin.value;
			version.minor = (uint)minor_spin.value;
			version.patch = (uint)patch_spin.value;
			project.version = version;
		}
		
		[GtkCallback]
		void on_path_entry_icon_press (Gtk.EntryIconPosition position, Gdk.Event event) {
			var dialog = new FileChooserDialog ("Choose place where save project", this, Gtk.FileChooserAction.SELECT_FOLDER);
			if (dialog.run() == Gtk.ResponseType.OK) {
				name_entry.text = dialog.get_file().get_basename();
				project = new Project (name_entry.text);
				project.sources = new string[]{"src/main.vala"};
				project.location = dialog.get_filename();
				path_entry.text = dialog.get_filename();
				set_page_complete (path_box, true);
			}
			dialog.destroy();
		}
		
		public Project project { get; private set; }
	}
}
