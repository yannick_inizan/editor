namespace Editor {
	public class ProjectView : Gtk.TreeView {

		Store store;

		public ProjectView (Gtk.Window window) {
			GLib.Object (window: window);
		}

		construct {
			store = new Store();
			provider = new CompletionProvider();
			headers_visible = false;
			insert_column_with_attributes (-1, "icon", new Gtk.CellRendererPixbuf(), "gicon", 0, null);
			insert_column_with_attributes (-1, "name", new Gtk.CellRendererText(), "text", 1, null);
			model = store;
			row_activated.connect (on_project_view_row_activated);
		}

		public Gtk.Window window { get; construct; }
		public Project project { get; set; }
		public CompletionProvider provider { get; set; }

		void on_project_view_row_activated (Gtk.TreePath path, Gtk.TreeViewColumn column) {
			Value icon, tree_type, filename;
			Gtk.TreeIter tmp;
			store.get_iter (out tmp, path);
			store.get_value (tmp, 0, out icon);
			store.get_value (tmp, 2, out tree_type);
			store.get_value (tmp, 3, out filename);

			if (path.get_indices ().length == 1 && path.get_indices()[0] == 0) {
				
			}
			else if (path.get_indices().length > 1 && path.get_indices()[0] == 0) {
				if ((TreeType)tree_type == TreeType.FILE)
					file_selected ((string)filename);
				if ((TreeType)tree_type == TreeType.UI)
					ui_selected ((string)filename);
				if ((TreeType)tree_type == TreeType.DIRECTORY) {
					var create_dialog = new Gtk.FileChooserDialog ("Add new file", window, Gtk.FileChooserAction.SAVE,
						"cancel", Gtk.ResponseType.CANCEL, "ok", Gtk.ResponseType.OK);
					if (create_dialog.run() == Gtk.ResponseType.OK) {
						if (create_dialog.get_filename().index_of (project.location) != 0) {
							var dialog = new Gtk.MessageDialog (create_dialog, Gtk.DialogFlags.MODAL, Gtk.MessageType.ERROR,
								Gtk.ButtonsType.OK, "path must starts with project location.");
							dialog.response.connect (id => { dialog.destroy(); });
							dialog.run();
						}
						else {
							var array = project.sources;
							print ("path: %s\n", create_dialog.get_filename());
							FileUtils.set_contents (create_dialog.get_filename(), "");
							array += create_dialog.get_filename().substring (1 + project.location.length);
							project.sources = array;
							update_tree (create_dialog.get_filename().substring (1 + project.location.length).split ("/"), src_iter);
							load_engine();
						}
					}
					create_dialog.destroy();
					
				}
			}
			else if (path.get_indices().length == 1 && path.get_indices()[0] == 1) {

			}
		}

		public signal void file_selected (string path);
		public signal void ui_selected (string ui);

		public void save() {
			project.save ();
		}
		
		Gtk.TreeIter src_iter;
		Gtk.TreeIter pkg_iter;

		public void load (Project project) {
			this.project = project;
			store.clear();
			store.append (out src_iter, null);
			store.append (out pkg_iter, null);
			store.set (src_iter, 0, new ThemedIcon ("folder"), 1, "Sources", 2, TreeType.DIRECTORY);
			foreach (string src in project.sources) {
				update_tree (src.split ("/"), src_iter);
			}
			store.set (pkg_iter, 0, new ThemedIcon ("package-x-generic"), 1, "Packages", 2, TreeType.NONE);
			foreach (string pkg in project.packages) {
				Gtk.TreeIter iter;
				store.append (out iter, pkg_iter);
				store.set (iter, 0, new ThemedIcon ("package-x-generic"), 1, pkg, 2, TreeType.PACKAGE);
			}
			load_engine();
		}
		
		void update_tree (string[] paths, Gtk.TreeIter parent, int depth = 0) {
			int num = store.iter_n_children (parent);
			Gtk.TreeIter? child = null;
			bool found = false;
			for (var i = 0; i < num; i++) {
				store.iter_nth_child (out child, parent, i);
				string name;
				store.get (child, 1, out name);
				if (str_equal (name, paths[depth])) {
					found = true;
					break;
				}
			}
			if (!found)
				store.append (out child, parent);
			string p = "";
			for (var j = 0; j <= depth; j++)
				p += "/" + paths[j];
			string location = project.location + "/" + p;
			print ("%s\n", location);
			if (!FileUtils.test (location, FileTest.EXISTS))
				return;
			bool is_dir = FileUtils.test (location, FileTest.IS_DIR);
			store.set (child, 0, new ThemedIcon (is_dir ? "folder" : "text-x-vala"), 1, paths[depth], 2, is_dir ? TreeType.DIRECTORY : TreeType.FILE, 3, location);
			if (depth < paths.length - 1)
				update_tree (paths, child, depth + 1);
		}

		void load_engine () {
			var thread = new Thread<void*> ("thread", () => {
				foreach (var src in project.sources) {
					var file = File.new_for_path (project.location + "/" + src);
					if (file.query_exists()) {
						FileInfo info = file.query_info ("standard::*", FileQueryInfoFlags.NONE);
						if (info.get_file_type() == FileType.DIRECTORY)
							continue;
						string ext = info.get_name().substring (info.get_name().last_index_of (".") + 1);
						if (ext != "vala" && ext != "vapi")
							continue;
						provider.set_source (project.location + "/" + src);
					}
				}
				foreach (string pkg in project.packages)
					provider.set_package (pkg);
				return null;
			});
		}
		
	}
}
