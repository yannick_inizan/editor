namespace Editor {
	public errordomain Error {
		NULL,
		INVALID,
		NOT_FOUND
	}
	
	public enum BuildType {
		NONE,
		CMAKE,
		AUTOTOOLS;
		
		public string get_nick() {
			string[] strv = {"none", "cmake", "autotools"};
			return strv[(int)this];
		}
		
		public static BuildType from_nick (string nick) {
			if (nick == "cmake")
				return BuildType.CMAKE;
			if (nick == "autotools")
				return BuildType.AUTOTOOLS;
			return BuildType.NONE;
		}
	}
	
	public class Project : GLib.Object {
		public class Version : GLib.Object {
			public uint major { get; set construct; }
			public uint minor { get; set construct; }
			public uint patch { get; set construct; }

			public string to_string() {
				return "%u.%u.%u".printf (major, minor, patch);
			}
		}

		public Project (string name) {
			GLib.Object (name: name);
		}

		public static Project load (string filename) throws GLib.Error {
			var parser = new MeeJson.Parser();
			parser.load_from_path (filename);
			if (!parser.root.is_object())
				throw new Error.INVALID ("invalid project format.");
			var o = parser.root.as_object();
			if (!o.has_key ("name"))
				throw new Error.NOT_FOUND ("project doesn't has valid name.");
			var prj = new Project (o["name"].as_string());
			prj.location = File.new_for_path (filename).get_parent().get_path ();
			if (!o.has_key ("version"))
				throw new Error.NOT_FOUND ("project doesn't has valid version.");
			var array = parser.root["version"].as_array();
			if (array.size != 3)
				throw new Error.INVALID ("invalid version. Expect 3 found %u".printf (array.size));
			uint major = (uint)array[0].as_int();
			uint minor = (uint)array[1].as_int();
			uint patch = (uint)array[2].as_int();
			var version = new Version();
			version.major = major;
			version.minor = minor;
			version.patch = patch;
			prj.version = version;
			if (o.has_key ("flags")) {
				if (!o["flags"].is_string_array())
					throw new Error.INVALID ("invalid type for flags");
				prj.flags = (string[])o["flags"].value;
			}
			if (o.has_key ("sources")) {
				if (!o["sources"].is_string_array())
					throw new Error.INVALID ("invalid type for sources");
				prj.sources = (string[])o["sources"].value;
			}
			if (o.has_key ("packages")) {
				string[] pkgs = o["packages"].as_string_array();
				foreach (var pkg in pkgs) {
					var list = Afrodite.Utils.get_package_paths (pkg);
					if (list == null || list.size == 0)
						throw new Error.NOT_FOUND ("can't find \"%s\" package.".printf (pkg));
				}
				prj.packages = pkgs;
			}
			if (!o.has_key ("build") || !o["build"].is_string())
				throw new Error.INVALID ("can't determine buildsystem type.");
			prj.build = BuildType.from_nick ((string)o["build"].value);
			return prj;
		}

		MeeJson.Object object;

		construct {
			object = new MeeJson.Object();
			object["name"] = name;
		}

		public void save() {
			var gen = new MeeJson.Generator();
			gen.root = new MeeJson.Node (object);
			gen.pretty = true;
			gen.indent = 1;
			gen.indent_char = '\t';
			FileUtils.set_contents (location + "/" + name + ".edi", gen.to_data());
		}

		public string location { get; set; }
		public string name { get; construct; }

		public bool is_library {
			get {
				if (object.has_key ("is-library") && object["is-library"].is_boolean())
					return (bool)object["is-library"].as_boolean();
				return false;
			}
			set {
				object["is-library"] = value;
			}
		}
		
		public BuildType build {
			get {
				if (object.has_key ("build") && object["build"].is_string())
					return BuildType.from_nick ((string)object["build"].value);
				return BuildType.NONE;
			}
			set {
				object["build"] = value.get_nick();
			}
		}

		public string[] flags {
			owned get {
				if (object.has_key ("flags") && object["flags"].is_array()) {
					var array = object["flags"].as_array();
					if (array.is_unique == MeeJson.NodeType.STRING)
						return (string[])object["flags"].value;
				}
				return new string[0];
			}
			set {
				object["flags"] = MeeJson.Array.from_strv (value);
			}
		}

		public string[] packages {
			owned get {
				if (object.has_key ("packages") && object["packages"].is_array()) {
					var array = object["packages"].as_array();
					if (array.is_unique == MeeJson.NodeType.STRING)
						return (string[])object["packages"].value;
				}
				return new string[0];
			}
			set {
				object["packages"] = MeeJson.Array.from_strv (value);
			}
		}
		
		public string[] sources {
			owned get {
				if (object.has_key ("sources") && object["sources"].is_array()) {
					var array = object["sources"].as_array();
					if (array.is_unique == MeeJson.NodeType.STRING)
						return (string[])object["sources"].value;
				}
				return new string[0];
			}
			set {
				object["sources"] = MeeJson.Array.from_strv (value);
			}
		}

		public Version version {
			owned get {
				if (!object.has_key ("version") || 
				    !object["version"].is_array() ||
				    !(object["version"].as_array().size != 3))
					return new Version();
				return new Version() {
					major = (uint)object["version"][0].as_int(),
					minor = (uint)object["version"][1].as_int(),
					patch = (uint)object["version"][2].as_int()
				};
			}
			set {
				var array = new MeeJson.Array();
				array.add (value.minor);
				array.add (value.major);
				array.add (value.patch);
				object["version"] = array;
			}
		}
	}
}
