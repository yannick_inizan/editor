namespace Editor {
	public class GladeView : Gtk.Box {
		public GladeView (string ui_path) {
			GLib.Object (path: ui_path, orientation: Gtk.Orientation.VERTICAL, spacing: 0);
		}
		
		construct {
			var project = Glade.Project.load (path);
			Glade.App.set_window (this);
			Glade.App.add_project (project);
			var palette = new Glade.Palette();
			palette.project = project;
			var inspector = new Glade.Inspector.with_project (project);
			var design_view = new Glade.DesignView (project);
			var editor = new Glade.Editor();
			var signal_editor = new Glade.SignalEditor();
			inspector.item_activated.connect (() => {
				var w = inspector.get_selected_items().nth_data (0);
				w.show();
				editor.load_widget (w);
				signal_editor.load_widget (w);
			});
			signal_editor.signal_activated.connect (sig => {
				print ("detail: %s, handler: %s\n", sig.detail, sig.handler);
			});
			pack_start (signal_editor);
			pack_start (inspector);
			pack_start (design_view);
			pack_start (editor);
			palette.show();
			inspector.show();
			design_view.show();
		}
		
		public string path { get; construct; }
	}
}
