namespace Editor {
	[GtkTemplate (ui = "/resources/Window.ui")]
	public class Window : Gtk.Window {
		[GtkChild]
		Gtk.Popover popover;
		[GtkChild]
		Gtk.Popover buildpopover;
		[GtkChild]
		Gtk.ScrolledWindow pv_scrolled_window;
		[GtkChild]
		Gtk.Notebook notebook;
		
		ProjectView project_view;
		Gee.ArrayList<Document> documents;
		Gee.HashMap<string, GladeView> glade_projects;

		int index_of_doc (string path) {
			int index = -1;
			for (var i = 0; i < documents.size; i++)
				if (documents[i].location == path)
					index = i;
			return -1;
		}

		construct {
			glade_projects = new Gee.HashMap<string, GladeView>();
			project_view = new ProjectView (this);
			project_view.ui_selected.connect (ui => {
				if (!glade_projects.has_key (ui)) {
					var view = new GladeView (ui);
					glade_projects[ui] = view;
					notebook.append_page (view, new Gtk.Label ("UI"));
					notebook.show_all();
				}
			});
			project_view.file_selected.connect (path => {
				int i = index_of_doc (path);
				Document doc = null;
				if (i == -1) {
					doc = new Document (path);
					doc.button_press_event.connect (event => {
						project_view.provider.document = doc;
						return false;
					});
					doc.view.completion.add_provider (project_view.provider);
					doc.saved.connect (() => {
						project_view.provider.set_source (doc.location);
					});
					documents.add (doc);
				}
				else
					doc = documents[i];
				notebook.append_page (doc, new Gtk.Label (doc.name));
				notebook.show_all();
			});
			pv_scrolled_window.add (project_view);
			documents = new Gee.ArrayList<Document>((d1, d2) => {
				return str_equal (d1.location, d2.location);
			});
		}

		
		[GtkCallback]
		void on_destroy() {
			Gtk.main_quit();
		}
		
		[GtkCallback]
		void on_buildlistbox_row_activated (Gtk.ListBoxRow row) {
			buildpopover.hide();
			string text = (row.get_child() as Gtk.Label).label;
			print ("text: %s\n", text);
			var build = Build.from (project_view.project);
			if (text == "Configure") {
				build.prepare();
				build.configure();
			}
			if (text == "Make")
				build.make();
			if (text == "Clean")
				build.clean();
		}
		
		[GtkCallback]
		void on_buildbutton_clicked() {
			buildpopover.show_all();
		}
		
		[GtkCallback]
		void on_listbox_row_activated (Gtk.ListBoxRow row) {
			popover.hide ();
			string text = (row.get_child () as Gtk.Label).label;
			if (text == "Quit")
				Gtk.main_quit ();
			if (text == "Open") {
				var dialog = new Gtk.FileChooserDialog ("Choose project", this, Gtk.FileChooserAction.OPEN,
					"cancel", Gtk.ResponseType.CANCEL, "ok", Gtk.ResponseType.OK);
				var filter = new Gtk.FileFilter();	
				filter.add_pattern ("*.edi");
				filter.add_pattern ("*.json");
				dialog.filter = filter;
				if (dialog.run() == Gtk.ResponseType.OK) {
					project_view.load (Project.load (dialog.get_filename()));
				}
				dialog.destroy();
			}
			if (text == "Save") {
				project_view.save();
			}
			if (text == "New") {
				var cw = new CreationWizard();
				cw.apply.connect (() => {
					print ("%s\n", cw.project.location);
					File.new_for_path (cw.project.location + "/src").make_directory();
					FileUtils.set_contents (cw.project.location + "/src/main.vala", "");
					cw.project.save();
					project_view.load (cw.project);
				});
				cw.cancel.connect (() => { cw.destroy(); });
				cw.close.connect (() => { cw.destroy(); });
				cw.show_all();
			}
		}

		[GtkCallback]
		void on_menubutton_clicked() {
			popover.show_all ();
		}
	}
}
