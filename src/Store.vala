namespace Editor {
	public enum TreeType {
		NONE, DIRECTORY, FILE, PACKAGE, UI
	}
	public class Store : Gtk.TreeStore {
		
		construct {
			set_column_types ({typeof (Icon), typeof (string), typeof (TreeType), typeof (string)});
		}

		public string get_pathname_at (Gtk.TreePath path) {
			if (path.get_depth() == 1)
				return "/";
			string pathname = "";
			while (path.get_depth() > 1) {
				Gtk.TreeIter iter;
				get_iter (out iter, path);
				Value val;
				get_value (iter, 1, out val);
				pathname = "/" + (string)val + pathname;
				path.up();
			}
			return pathname;
		}
	}
}
