namespace Editor {
	public class Package : GLib.Object {
		public Package (string name, string info) {
			GLib.Object (name: name, info: info);
		}
		
		public string info { get; construct; }
		public string name { get; construct; }
		
		public static Gee.ArrayList<Package> load() {
			string output;
			string err;
			Process.spawn_command_line_sync ("pkg-config --list-all", out output, out err);
			var list = new Gee.ArrayList<Package>();
			foreach (string line in output.split ("\n")) {
				if (line.length < 1)
					continue;
				string name = line.substring (0, line.index_of (" "));
				string info = line.substring (line.index_of (" ")).strip();
				list.add (new Package (name, info));
			}
			list.add (new Package ("posix", "Posix api"));
			list.add (new Package ("linux", "Linux api"));
			list.sort ((p1, p2) => {
				return strcmp ((p1 as Package).name, (p2 as Package).name);
			});
			return list;
		}
	}
	
	public class PackageView : Gtk.TreeView {
		Gtk.ListStore store;
		
		public signal void selection_changed();
		
		construct {
			store = new Gtk.ListStore (3, typeof (bool), typeof (string), typeof (string));
			model = store;
			var toggle = new Gtk.CellRendererToggle();
			toggle.toggled.connect (path => {
				Gtk.TreeIter iter;
				store.get_iter_from_string (out iter, path);
				store.set (iter, 0, !toggle.active);
				selection_changed();
			});
			insert_column_with_attributes (-1, null, toggle, "active", 0);
			insert_column_with_attributes (-1, null, new Gtk.CellRendererText(), "text", 1);
			insert_column_with_attributes (-1, null, new Gtk.CellRendererText(), "text", 2);
			foreach (var pkg in Package.load()) {
				Gtk.TreeIter iter;
				store.append (out iter);
				store.set (iter, 1, pkg.name, 2, pkg.info);
			}
		}
		
		public string[] packages {
			owned get {
				string[] pkgs = new string[0];
				store.foreach ((model, path, iter) => {
					Value pkg, active;
					model.get_value (iter, 1, out pkg);
					model.get_value (iter, 0, out active);
					if ((bool)active)
						pkgs += (string)pkg;
					return false;
				});
				return pkgs;
			}
		}
		
		void set_package (string pkg) {
			store.foreach ((model, path, iter) => {
				Value val;
				model.get_value (iter, 1, out val);
				if ((string)val == pkg)
					store.set_value (iter, 0, true);
				return true;
			});
		}
		
		void unset_package (string pkg) {
			store.foreach ((model, path, iter) => {
				Value val;
				model.get_value (iter, 1, out val);
				if ((string)val == pkg)
					store.set_value (iter, 0, false);
				return true;
			});
		}
		
		public void load (Project project) {
			foreach (var pkg in project.packages)
				set_package (pkg);
		}
	}
}
