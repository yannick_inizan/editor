namespace Editor {
	public class Document : Gtk.ScrolledWindow {

		public Document (string location) {
			GLib.Object (location: location);
		}

		construct {
			var buffer = new Gtk.SourceBuffer.with_language (new Gtk.SourceLanguageManager().get_language ("vala"));
			uint8[] data;
			File.new_for_path (location).load_contents (null, out data, null);
			buffer.text = (string)data;
			view = new Gtk.SourceView.with_buffer (buffer);
			add (view);

			key_press_event.connect (event => {
				Gtk.TextIter iter;
				view.buffer.get_iter_at_mark (out iter, view.buffer.get_insert ());
				line = iter.get_line();
				column = iter.get_line_offset();
				if ((event.state & Gdk.ModifierType.CONTROL_MASK) > 0 && event.keyval == Gdk.Key.s) {
					FileUtils.set_contents (location, view.buffer.text);
					saved();
				}
				return true;
			});
		}

		public signal void saved();

		public Gtk.SourceView view { get; private set; }
		public int column { get; private set; }
		public int line { get; private set; }
		public string location { get; construct; }
		public string name {
			owned get {
				return location.split ("/")[location.split ("/").length - 1];
			}
		}
	}
}
