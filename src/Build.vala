namespace Editor {
	public errordomain BuildError {
		NONE,
		WARNING,
		FAILED
	}

	public abstract class Build : GLib.Object {
		public abstract void clean() throws BuildError;
		public abstract void configure() throws BuildError; 
		public abstract void make() throws BuildError;
		public abstract void prepare();
		
		public Project project { get; construct; }
		
		public static Build? from (Project project) {
			if (project.build == BuildType.AUTOTOOLS)
				return new Autotools (project);
			return null;
		}
	}
	
	public class Autotools : Build {
		public Autotools (Project project) {
			GLib.Object (project: project);
		}
		
		public override void prepare() {
			var m4 = File.new_for_path (project.location + "/m4");
			if (!m4.query_exists())
				m4.make_directory_with_parents();
			var sh = File.new_for_path (project.location + "/autogen.sh");
			if (sh.query_exists())
				sh.delete();
			sh.create (FileCreateFlags.NONE).write ("""#!/bin/sh
set -ex

autoreconf -f -i""".data);
			string output;
			string err;
			Process.spawn_command_line_sync ("chmod +x %s/autogen.sh".printf (project.location), out output, out err);
			var cac = File.new_for_path (project.location + "/configure.ac");
			if (cac.query_exists())
				cac.delete();
			string pkgs = "";
			foreach (string pkg in project.packages) {
				if (pkg == "linux" || pkg == "posix")
					continue;
				pkgs += "%s ".printf (pkg);
			}
			string vpkgs = "";
			foreach (string pkg in project.packages)
				vpkgs += "--pkg %s ".printf (pkg);
			string srcs = "";
			foreach (string src in project.sources)
				srcs += "\\\n	%s ".printf (src);
			string data = """AC_PREREQ([2.65])
AC_INIT([%s], [%s], [username@dummy.com], [%s])
AC_CONFIG_MACRO_DIR([m4])
AM_INIT_AUTOMAKE([subdir-objects])
AC_CONFIG_FILES([Makefile])

AM_PROG_AR
LT_INIT
AC_PROG_CC
AM_PROG_VALAC

PKG_CHECK_MODULES(%s, [%s])
AC_SUBST(%s_CFLAGS)
AC_SUBST(%s_LIBS)


AC_OUTPUT""".printf (project.name, project.version.to_string(), project.name,
project.name.up(), pkgs, project.name.up(), project.name.up());
			cac.create (FileCreateFlags.NONE).write (data.data);
			var mam = File.new_for_path (project.location + "/Makefile.am");
			if (mam.query_exists())
				mam.delete();
			mam.create (FileCreateFlags.NONE).write ("""ACLOCAL_AMFLAGS = -I m4
EXTRA_DIST = autogen.sh

DISTCLEANFILES = aclocal.m4 ar-lib compile config.guess config.sub config.status configure \
	depcomp install-sh libtool ltmain.sh missing Makefile.in

distclean-local:
	rm -rf autom4te.cache

bin_PROGRAMS = %s

%s_CFLAGS = $(%s_CFLAGS)

%s_SOURCES = %s

%s_LDADD = $(%s_LIBS)
%s_VALAFLAGS = %s

CLEANFILES = *.c *.o *.stamp %s""".printf (project.name,
	project.name.replace ("-", "_"), project.name.replace ("-", "_").up(),
	project.name.replace ("-", "_"), srcs,
	project.name.replace ("-", "_"), project.name.replace ("-", "_").up(),
	project.name.replace ("-", "_"), vpkgs,
	project.name).data);
	
			
			Environment.set_current_dir (project.location);
			Process.spawn_command_line_sync ("./autogen.sh", null, null);
		}
		
		public override void clean() throws BuildError {
			string output;
			string err;
			Environment.set_current_dir (project.location);
			Process.spawn_command_line_sync ("make clean", out output, out err);
			if (err != null && err.length > 1)
				throw new BuildError.FAILED (err);
		}
		
		public override void configure() throws BuildError {
			string err = null;
			Environment.set_current_dir (project.location);
			Process.spawn_command_line_sync ("./configure", null, out err);
			if (err != null && err.length > 1)
				throw new BuildError.FAILED (err);
		}
		
		public override void make() throws BuildError {
			string output;
			string err;
			Environment.set_current_dir (project.location);
			Process.spawn_command_line_sync ("make", out output, out err);
			if (err != null && err.length > 1)
				throw new BuildError.FAILED (err);
		}
	}
}
